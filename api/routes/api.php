<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/



Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['middleware' => ['jwt.verify']], function() {
    Route::resource('ativo','AtivoController');
    Route::resource('usuario','UsuarioController');
    Route::resource('setor','SetorController');
    Route::resource('usuarioxativo','UsuarioXAtivoController');
});

Route::post('novousuario','UsuarioController@store');
Route::post('auth/login','AutorizacaoController@login');
Route::post('auth/logout','AutorizacaoController@logout');