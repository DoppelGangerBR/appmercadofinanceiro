<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ativo extends Model
{
    protected $table = 'ativo';
    protected $primaryKey = 'CodAtivo';
    protected $fillable = ['CodAtivo','Ticker','Nome','CodSetor','CodTipoAtivo'];
}
