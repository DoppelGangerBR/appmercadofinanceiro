<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Setor extends Model
{
    protected $table = 'setor';
    protected $primaryKey = 'CodSetor';
    protected $fillable = ['CodSetor','Nome'];
}
