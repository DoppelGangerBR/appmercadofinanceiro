<?php

namespace App\Http\Controllers;

use App\Ativo;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
class AtivoController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //
        $codTipoAtivo = $request->header('CodTipoAtivo');
        return DB::select(DB::raw(
            'select * from ativo where "CodTipoAtivo" = '.$codTipoAtivo
        ));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Ativo  $ativo
     * @return \Illuminate\Http\Response
     */
    public function show(Ativo $ativo)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Ativo  $ativo
     * @return \Illuminate\Http\Response
     */
    public function edit(Ativo $ativo)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Ativo  $ativo
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Ativo $ativo)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Ativo  $ativo
     * @return \Illuminate\Http\Response
     */
    public function destroy(Ativo $ativo)
    {
        //
    }
}
