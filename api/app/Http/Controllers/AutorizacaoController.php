<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Usuario;
use Validator;
use Hash;
use JWTAuth;
use JWTGuard;
class AutorizacaoController
{
    public function login(Request $request){
        $validator = Validator::make($request->all(),[
            'Email' => 'required|regex:/^.+@.+$/i',
            'Senha' => 'required|min:6'
        ]);
        if($validator->fails()){
            return response()->json(['Status'=>false,"Erros" => $validator->errors()],400);
        }else{
            $credenciais = $request->only('Email','Senha');
            $usuario = Usuario::where('Email', $credenciais['Email'])->first();
            if(!$usuario){
                return response()->json([
                    'Status'=>false,
                    'Msg' => 'Usuario não encontrado'
                ], 401);
            }
            if($usuario['Ativo'] == false){
                return response()->json([
                    'Status'=>false,
                    'Msg' => 'Usuario desativado, por favor, contate-nos'
                ], 401);
            }
            if(!Hash::check($credenciais['Senha'], $usuario->Senha)){
                return response()->json([
                    'Status'=>false,
                    'Msg' => 'Senha invalida!'
                ], 401);
            }
            $token = JWTAuth::fromUser($usuario);
            $objectToken = JWTAuth::setToken($token);
            $usuario->Token = $token;
            $usuario->save();
            return response()->json([
                'Token' => $token,
                'CodUsuario'=>$usuario['CodUsuario'],
                'Status'=>true
            ]);
        }
    }
    public function logout(){
        JWTAuth::invalidate();
        return response([
            'Status'=>true,
            'Msg' => 'Logout com sucesso'
        ],200);
    }
}
