<?php

namespace App\Http\Controllers;

use App\Usuario;
use Illuminate\Http\Request;
use Validator;
use Hash;
use App;
class UsuarioController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'Senha' => 'required|min:6',
            'Email' => 'required|unique:usuario|regex:/^.+@.+$/i'
        ]);
        if($validator->fails()){
            return response()->json(['Status'=>false,"Erros" => $validator->errors()],400);
        }else{
            $usuario = $request->all();
            $usuario['Senha'] = Hash::make($usuario['Senha']);
            try{
                Usuario::create($usuario);
                return response()->json(['Status'=>true,'Msg'=>'Usuário criado com sucesso!'],200);
            }catch(\Exception $e){
                return response()->json(['Status'=>false,'Msg'=>'Falha ao se cadastrar, por favor, tente novamente mais tarde!'],500);
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Usuario  $usuario
     * @return \Illuminate\Http\Response
     */
    public function show(Usuario $usuario)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Usuario  $usuario
     * @return \Illuminate\Http\Response
     */
    public function edit(Usuario $usuario)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Usuario  $usuario
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Usuario $usuario)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Usuario  $usuario
     * @return \Illuminate\Http\Response
     */
    public function destroy(Usuario $usuario)
    {
        //
    }
}
