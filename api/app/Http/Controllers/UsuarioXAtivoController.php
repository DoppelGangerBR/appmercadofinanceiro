<?php

namespace App\Http\Controllers;

use App\UsuarioXAtivo;
use Illuminate\Http\Request;

class UsuarioXAtivoController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\UsuarioXAtivo  $usuarioXAtivo
     * @return \Illuminate\Http\Response
     */
    public function show(UsuarioXAtivo $usuarioXAtivo)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\UsuarioXAtivo  $usuarioXAtivo
     * @return \Illuminate\Http\Response
     */
    public function edit(UsuarioXAtivo $usuarioXAtivo)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\UsuarioXAtivo  $usuarioXAtivo
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, UsuarioXAtivo $usuarioXAtivo)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\UsuarioXAtivo  $usuarioXAtivo
     * @return \Illuminate\Http\Response
     */
    public function destroy(UsuarioXAtivo $usuarioXAtivo)
    {
        //
    }
}
