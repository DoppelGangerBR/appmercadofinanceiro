﻿using calculadora.Views;
using System.Windows.Input;
using Xamarin.Forms;

namespace calculadora.ViewModel
{
    class AdicionarAtivoPasso1ViewModel
    {
        private readonly Page view;
        public AdicionarAtivoPasso1ViewModel()
        {
            view = Application.Current.MainPage;
        }
        public ICommand AddAcao => new Command(async () =>
        {
            await view.Navigation.PushAsync(new AdicionarAtivoPasso2(1));
        });
        public ICommand AddFii => new Command(async () =>
        {
            await view.Navigation.PushAsync(new AdicionarAtivoPasso2(2));
        });
    }
}
