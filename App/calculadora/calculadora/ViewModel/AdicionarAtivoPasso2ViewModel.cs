﻿using calculadora.Model;
using calculadora.Utils;
using calculadora.Views;
using Rg.Plugins.Popup.Services;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using Xamarin.Forms;

namespace calculadora.ViewModel
{
    class AdicionarAtivoPasso2ViewModel : INotifyPropertyChanged
    {
        private readonly Page pagina;
        private Ativo _ativoSelecionado;
        private UsuarioXAtivo usuarioXAtivo;
        private ObservableCollection<Ativo> _listaAtivos;
        private readonly FormataMoeda formataMoeda;
        private readonly int CodTipoAtivo;
        private string _tituloPagina;
        private DateTime _dataCompra = DateTime.Today;

        public event PropertyChangedEventHandler PropertyChanged;

        public AdicionarAtivoPasso2ViewModel(int CodTipoAtivo)
        {
            pagina = Application.Current.MainPage;
            this.CodTipoAtivo = CodTipoAtivo;
            _listaAtivos = new ObservableCollection<Ativo>();
            usuarioXAtivo = new UsuarioXAtivo();
            formataMoeda = new FormataMoeda();
            CarregaListaAtivos();
            DefineTituloPagina();
        }

        private void DefineTituloPagina()
        {
            switch (CodTipoAtivo)
            {
                case 1:
                    TituloPagina = "Ações";
                    break;
                case 2:
                    TituloPagina = "FII's";
                    break;
            }
        }

        private async void CarregaListaAtivos()
        {
            Conexao api = new Conexao();
            dynamic ativos = null;
            PopUp popup = new PopUp();
            popup.IniciaIndicador("Carregando ativos");
            await PopupNavigation.Instance.PushAsync(popup);
            try
            {
                ativos = await api.Get("ativo", new List<Cabecalho>() { new Cabecalho() {Key = "CodTipoAtivo", Value = this.CodTipoAtivo.ToString() }});

            }catch(Exception e)
            {
                ativos = null;
                await pagina.DisplayAlert("Erro", "Falha ao carregar lista de ativos, por favor verifique sua conexão com a internet", "OK");
            }
            if(ativos != null)
            {
                foreach(var ativo in ativos)
                {
                    Ativos.Add(new Ativo()
                    {
                        CodAtivo = ativo.CodAtivo,
                        Nome = ativo.Nome,
                        Ticker = ativo.Ticker
                    });
                }
            }
            await PopupNavigation.Instance.PopAsync();
        }

        public ObservableCollection<Ativo> Ativos
        {
            get
            {
                return _listaAtivos;
            }
            set
            {
                _listaAtivos = value;
            }
        }
        public Ativo AtivoSelecionado { 
            get
            {
                return _ativoSelecionado;
            }
            set
            {
                _ativoSelecionado = value;
                if(_ativoSelecionado != null)
                {
                    Nome = value.Nome;
                }
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(AtivoSelecionado)));
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(Nome)));
            }

        }
        public string Nome
        {
            get
            {
                if(AtivoSelecionado != null)
                {
                    return AtivoSelecionado.Nome;
                }
                return "";
            }
            set
            {
                if(AtivoSelecionado != null)
                {
                    AtivoSelecionado.Nome = value;
                }
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(Nome)));
            }
        }
        public int Quantidade
        {
            get
            {
                return usuarioXAtivo.Quantidade;
            }
            set
            {
                usuarioXAtivo.Quantidade = Quantidade;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(Quantidade)));
            }
        }
        public int CodUsuario
        {
            get
            {
                return usuarioXAtivo.CodUsuario;
            }
            set
            {
                usuarioXAtivo.Quantidade = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(CodUsuario)));
            }
        }
        public string ValorUnitario
        {
            get
            {
                return formataMoeda.FormataValor(usuarioXAtivo.ValorUnitario);
            }
            set
            {
                usuarioXAtivo.ValorUnitario = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(ValorUnitario)));
            }
        }
        public DateTime DataCompra
        {
            get
            {
                return _dataCompra;
            }
            set
            {
                _dataCompra = value;
                usuarioXAtivo.DataCompra = value.ToString("dd/MM/yyyy");
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(DataCompra)));
            }
        }

        public string TituloPagina
        {
            get
            {
                return _tituloPagina;
            }
            set
            {
                _tituloPagina = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(TituloPagina)));
            }
        }
    }
}
