﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows.Input;
using Xamarin.Forms;
using calculadora.Model;
namespace calculadora.ViewModel
{
    class CalculadoraFIViewModel : INotifyPropertyChanged
    {
        Opcoes opcoes;
        Opcoes _opcaoSelecionada;
        public CalculadoraFIViewModel()
        {
            opcoes = new Opcoes();
        }
        public ICommand Calcula => new Command(() => {
            double vlrInicial, investMensal, qtdMeses, vlrFinal, juros;
            vlrInicial = double.Parse(TxtValorInicial);
            investMensal = double.Parse(TxtValorMensal);
            qtdMeses = Int32.Parse(TxtQtdMeses);
            juros = double.Parse(TxtRendimentoDiario.Replace(",","."));
            vlrFinal = vlrInicial;
            double valorAtual = 0.0;
            for (int mes = 0; mes < qtdMeses; mes++)
            {
                if(_opcaoSelecionada.CodOpcao == 2)
                {
                    for (int dia = 0; dia < 21; dia++)
                    {
                        valorAtual = vlrFinal;
                        vlrFinal += vlrFinal * (juros / 100);
                    }
                }
                else
                {
                    valorAtual = vlrFinal;
                    vlrFinal += vlrFinal * (juros / 100);
                }
                if (mes != qtdMeses - 1)
                {
                    vlrFinal = vlrFinal+ investMensal;
                }
            }
            TxtValorFinal = string.Format("{0:0,0.00} R$", vlrFinal);
        });
        private string _txtValorInicial, _txtValorMensal, _txtQtdMeses, _txtValorFinal;
        private string _txtRendimentoDiario;

        public event PropertyChangedEventHandler PropertyChanged;

        public string TxtValorInicial
        {
            get
            {
                return _txtValorInicial;
            }
            set
            {
                _txtValorInicial = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(TxtValorInicial)));
            }
        }
        public string TxtValorMensal
        {
            get
            {
                return _txtValorMensal;
            }
            set
            {
                _txtValorMensal = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(TxtValorMensal)));
            }
        }
        public string TxtRendimentoDiario
        {
            get
            {
                return _txtRendimentoDiario;
            }
            set
            {
                _txtRendimentoDiario = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(TxtRendimentoDiario)));
            }
        }
        public string TxtQtdMeses
        {
            get
            {
                return _txtQtdMeses;
            }
            set
            {
                _txtQtdMeses = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(TxtQtdMeses)));
            }
        }
        public string TxtValorFinal
        {
            get
            {
                return _txtValorFinal;
            }
            set
            {
                _txtValorFinal = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(TxtValorFinal)));
            }
        }
        public Opcoes OpcaoSelecionada
        {
            get
            {
                return _opcaoSelecionada;
            }
            set
            {
                _opcaoSelecionada = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(OpcaoSelecionada)));
            }
        }
    }
}
