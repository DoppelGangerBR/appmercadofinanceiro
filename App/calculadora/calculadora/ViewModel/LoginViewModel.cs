﻿using calculadora.Model;
using calculadora.Utils;
using calculadora.Views;
using Rg.Plugins.Popup.Services;
using System;
using System.ComponentModel;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace calculadora.ViewModel
{
    public class LoginViewModel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        private readonly Usuario usuario;
        private readonly Page view;
        private bool _lembrarEmail = false;
        public LoginViewModel()
        {
            usuario = new Usuario();
            view = Application.Current.MainPage;
            if (Application.Current.Properties.ContainsKey("LembrarEmail"))
            {
                _lembrarEmail = Boolean.Parse(Application.Current.Properties["LembrarEmail"].ToString());
            }
            if (Application.Current.Properties.ContainsKey("Email") && _lembrarEmail)
            {
                Email = Application.Current.Properties["Email"].ToString().Replace("\n", "");
            }
        }
        public ICommand Entrar => new Command(async() =>
        {
            dynamic resposta = null;
            try
            {
                PopUp popup = new PopUp();
                popup.IniciaIndicador("Entrando...");
                await PopupNavigation.Instance.PushAsync(popup);
                Conexao api = new Conexao();
                await Task.Run(async () =>
                {
                    resposta = await api.Post("auth/login", usuario);
                });
                await PopupNavigation.Instance.PopAsync();
                if(resposta.Status == true)
                {
                    Usuario.CodUsuario = resposta.CodUsuario;
                    Usuario.Token = resposta.Token;
                    await view.Navigation.PushAsync(new Carteira());
                }
                else
                {
                    await view.DisplayAlert("Erro", new Validacoes().ValidaLoginApi(resposta), "OK");
                }
            }catch(Exception e)
            {
                await view.DisplayAlert("Erro", "Falha ao conectar, por favor, verifique sua conexão com a internet e tente novamente!", "OK");
            }
        });
        

        public bool LembrarEmail
        {
            get
            {
                return _lembrarEmail;
            }
            set
            {
                _lembrarEmail = value;
                Application.Current.Properties["LembrarEmail"] = value;
                if (LembrarEmail && !string.IsNullOrEmpty(Email))
                {
                    Application.Current.Properties["Email"] = Email;
                }
                else
                {
                    Application.Current.Properties.Remove("Email");
                    Application.Current.Properties["LembraEmail"] = false;
                }
                Application.Current.SavePropertiesAsync();
            }
        }
        public ICommand Cadastrar => new Command(async () =>
        {
            await view.Navigation.PushAsync(new Cadastro());
        });
        public string Senha
        {
            get
            {
                return usuario.Senha;
            }
            set
            {
                usuario.Senha = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(Senha)));
            }
        }
        public string Email
        {
            get
            {
                return usuario.Email;
            }
            set
            {
                usuario.Email = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(Email)));
            }
        }

        public int CodUsuario
        {
            get
            {
                return Usuario.CodUsuario;
            }
            set
            {
                Usuario.CodUsuario = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(CodUsuario)));
            }
        }
        public string Token
        {
            set
            {
                Usuario.Token = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(Token)));
            }
        }
    }
}
