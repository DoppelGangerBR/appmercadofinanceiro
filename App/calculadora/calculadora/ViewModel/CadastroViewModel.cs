﻿using calculadora.Model;
using calculadora.Utils;
using System;
using System.ComponentModel;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace calculadora.ViewModel
{
    class CadastroViewModel : INotifyPropertyChanged
    {
        private readonly Usuario usuario;
        private readonly Page view;
        public CadastroViewModel()
        {
            usuario = new Usuario();
            view = Application.Current.MainPage;
        }
        public ICommand Cadastrar => new Command(async () =>
        {
            Conexao api = new Conexao();
            dynamic resposta = null;
            if (await ValidaCamposObrigatorios())
            {
                try
                {
                    resposta = await api.Post("novousuario", usuario);
                    if (resposta.Status == true)
                    {
                        await view.DisplayAlert("Sucesso", "Usuário cadastrado com sucesso!", "OK");
                        await view.Navigation.PopAsync();
                    }
                    else
                    {
                        await view.DisplayAlert("Erro", new Validacoes().ValidaCadastroApi(resposta), "OK");
                    }
                }
                catch (Exception e)
                {
                    await view.DisplayAlert("Falha", "Falha ao se cadastrar, verifique sua conexão com a internet e tente novamente", "OK");
                }
            }
        });

        private async Task<bool> ValidaCamposObrigatorios()
        {
            bool senhasIguais = false;
            bool emailsIguais = false;
            string mensagemErro = "Os seguintes erros ocorreram:\n";
            if (!string.IsNullOrEmpty(Email))
            {
                if (Email.Equals(ConfirmacaoEmail, StringComparison.CurrentCultureIgnoreCase))
                {
                    emailsIguais = true;
                }
                else
                {
                    mensagemErro += "Emails não são iguais";
                }
            }
            else
            {
                await view.DisplayAlert("Erro", "Por favor, informe um Email", "OK");
                return false;
            }
            if (!string.IsNullOrEmpty(Senha))
            {
                if (Senha.Equals(ConfirmacaoSenha, StringComparison.CurrentCultureIgnoreCase))
                {
                    senhasIguais = true;
                }
                else
                {
                    mensagemErro += "Senhas não são iguais\n";
                }
            }
            else
            {
                await view.DisplayAlert("Erro", "Por favor, informe uma senha", "OK");
                return false;
            }

            

            if (senhasIguais && emailsIguais)
            {
                return true;
            }
            await view.DisplayAlert("Erro", mensagemErro, "OK");
            return false;
        }
        private string _confirmacaoSenha, _confirmacaoEmail;
        public string Senha
        {
            get
            {
                return usuario.Senha;
            }
            set
            {
                usuario.Senha = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(Senha)));
            }
        }
        public string ConfirmacaoSenha
        {
            get
            {
                return _confirmacaoSenha;
            }
            set
            {
                _confirmacaoSenha = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(ConfirmacaoSenha)));
            }
        }
        public string Email
        {
            get
            {
                return usuario.Email;
            }
            set
            {
                usuario.Email = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(Email)));
            }
        }
        public string ConfirmacaoEmail
        {
            get
            {
                return _confirmacaoEmail;
            }
            set
            {
                _confirmacaoEmail = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(ConfirmacaoEmail)));
            }
        }
        public int CodUsuario
        {
            get
            {
                return Usuario.CodUsuario;
            }
            set
            {
                Usuario.CodUsuario = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(CodUsuario)));
            }
        }
        public string Token
        {
            set
            {
                Usuario.Token = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(Token)));
            }
        }
        public event PropertyChangedEventHandler PropertyChanged;
    }
}
