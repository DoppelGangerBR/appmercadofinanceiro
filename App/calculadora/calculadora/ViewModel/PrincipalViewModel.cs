﻿using System.Windows.Input;
using Xamarin.Forms;
using calculadora.Views;
using calculadora.Model;

namespace calculadora.ViewModel
{
    public class PrincipalViewModel
    {
        public PrincipalViewModel()
        {

        }
        public ICommand AbreCalculadoraFundoInvestimento => new Command(async () =>
        {
            await Application.Current.MainPage.Navigation.PushAsync(new CalculadoraFI());
        });

        public ICommand AbreCalculadoraFundoImbobiliario => new Command(async () =>
        {
            await Application.Current.MainPage.Navigation.PushAsync(new CalculadoraFImobiliario());
        });

        public ICommand AbreMinhaCarteira => new Command(async () =>
        {
            if(Usuario.EstaLogado || Usuario.PermanecerLogado())
            {
                await Application.Current.MainPage.Navigation.PushAsync(new Carteira());
            }
            else
            {
                await Application.Current.MainPage.Navigation.PushAsync(new Login());
            }
        });
    }
}

