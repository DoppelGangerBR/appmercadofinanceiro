﻿using calculadora.Model;
using calculadora.Views;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows.Input;
using Xamarin.Forms;

namespace calculadora.ViewModel
{
    class CarteiraViewModel : INotifyPropertyChanged
    {
        private readonly Page pagina;
        public CarteiraViewModel()
        {
            pagina = Application.Current.MainPage;
        }

        public ICommand AdicionarAtivo => new Command(async() =>
        {
            await pagina.Navigation.PushAsync(new AdicionarAtivoPasso1());
        });

        private Ativo _ativoSelecionado;

        public event PropertyChangedEventHandler PropertyChanged;

        public List<Ativo> AtivosDoUsuario { get; set; }
    }
}
