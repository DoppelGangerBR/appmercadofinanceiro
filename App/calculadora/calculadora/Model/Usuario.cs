﻿using System;
using Xamarin.Forms;

namespace calculadora.Model
{
    public class Usuario
    {
        public static int CodUsuario { get; set; }
        public string Senha { get; set; }
        public static string Token { get; set; }
        public string Email { get; set; }

        public static bool EstaLogado { get; set; }


        public static bool PermanecerLogado()
        {
            try
            {
                return Boolean.Parse(Application.Current.Properties["PermanecerLogado"].ToString().Replace("\n", ""));
            }
            catch (Exception)
            {
                return false;
            }
        }

    }
}
