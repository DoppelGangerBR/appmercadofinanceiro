﻿namespace calculadora.Model
{
    public class UsuarioXAtivo
    {
        public int CodAtivo { get; set; }
        public int CodUsuario { get; set; }
        public int Quantidade { get; set; }
        public string ValorUnitario { get; set; }
        public string DataCompra { get; set; }
    }
}
