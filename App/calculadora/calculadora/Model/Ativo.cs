﻿namespace calculadora.Model
{
    public class Ativo
    {
        public int CodAtivo { get; set; }
        public string Ticker { get; set; }
        public string Nome { get; set; }
        public double Diferenca { get; set; }
        public float Rentabilidade { get; set; }
        public float ValorAbertura { get; set; }
        public float ValorFechamentoAnterior { get; set; }
        public int CodSetor { get; set; }
    }
}
