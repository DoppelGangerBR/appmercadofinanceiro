﻿using Newtonsoft.Json;
namespace calculadora.Model
{
    public static class JsonUtils
    {
        public static string ToJson(this object objeto)
        {
            return JsonConvert.SerializeObject(objeto);
        }
    }
}
