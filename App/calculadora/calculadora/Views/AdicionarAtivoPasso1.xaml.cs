﻿using calculadora.ViewModel;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace calculadora.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class AdicionarAtivoPasso1 : ContentPage
    {
        public AdicionarAtivoPasso1()
        {
            InitializeComponent();
            BindingContext = new AdicionarAtivoPasso1ViewModel();
        }
    }
}