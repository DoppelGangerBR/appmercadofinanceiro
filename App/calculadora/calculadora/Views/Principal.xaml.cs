﻿using calculadora.ViewModel;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace calculadora.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Principal : ContentPage
    {
        public Principal()
        {
            InitializeComponent();
            BindingContext = new PrincipalViewModel();
        }
    }
}