﻿using calculadora.ViewModel;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using System.Linq;
namespace calculadora.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Cadastro : ContentPage
    {
        public Cadastro()
        {
            InitializeComponent();
            BindingContext = new CadastroViewModel();
        }

        private void Entry_Completed(object sender, System.EventArgs e)
        {
            Entry entry = (Entry)sender;
            int proximoIndex = entry.TabIndex + 1;
            Entry proximaEntry = (Entry)stkCadastro.Children.FirstOrDefault(s => s.TabIndex == proximoIndex);
            if(proximaEntry != null)
            {
                proximaEntry.Focus();
            }
        }
    }
}