﻿using calculadora.ViewModel;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace calculadora.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class CalculadoraFI : ContentPage
    {
        public CalculadoraFI()
        {
            InitializeComponent();
            BindingContext = new CalculadoraFIViewModel();
        }
    }
}