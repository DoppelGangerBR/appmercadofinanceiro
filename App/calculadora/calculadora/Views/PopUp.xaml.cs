﻿using Rg.Plugins.Popup.Pages;
using Xamarin.Forms.Xaml;

namespace calculadora.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class PopUp : PopupPage
    {
        public PopUp()
        {
            InitializeComponent();
        }
        public void IniciaIndicador(string mensagem)
        {
            lblInformacao.Text = mensagem;
        }

        private void PopupPage_BackgroundClicked(object sender, System.EventArgs e)
        {

        }
    }
}