﻿using calculadora.ViewModel;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace calculadora.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Carteira : ContentPage
    {
        public Carteira()
        {
            InitializeComponent();
            BindingContext = new CarteiraViewModel();
        }
    }
}