﻿using calculadora.ViewModel;
using System.Linq;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace calculadora.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Login : ContentPage
    {
        public Login()
        {
            InitializeComponent();
            BindingContext = new LoginViewModel();
        }

        private void Entry_Completed(object sender, System.EventArgs e)
        {
            Entry entry = (Entry)sender;
            int proximoIndex = entry.TabIndex + 1;
            Entry proximaEntry = (Entry)stkLogin.Children.FirstOrDefault(s => s.TabIndex == proximoIndex);
            if (proximaEntry != null)
            {
                proximaEntry.Focus();
            }
        }
    }
}