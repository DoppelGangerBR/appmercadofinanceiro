﻿using calculadora.Model;
using calculadora.ViewModel;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace calculadora.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class AdicionarAtivoPasso2 : ContentPage
    {
        public AdicionarAtivoPasso2(int CodTipoAtivo)
        {
            InitializeComponent();
            BindingContext = new AdicionarAtivoPasso2ViewModel(CodTipoAtivo);
        }

    }
}