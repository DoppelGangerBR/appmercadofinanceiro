﻿using calculadora.Model;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace calculadora.Utils
{
    public class Conexao
    {
        private TimeSpan timeout = TimeSpan.FromSeconds(20);
        private readonly string Token;
        private readonly string UrlServidor;
        public Conexao()
        {
            try
            {
                Token = Usuario.Token;

            }catch(Exception)
            {
                Token = null;
            }
            try
            {
                UrlServidor = Constantes.UrlApi;

            }
            catch (Exception)
            {
                UrlServidor = null;
            }
        }
        
        public async Task<dynamic> Post(string rota, object objeto)
        {
            HttpClient cliente = new HttpClient();
            cliente.Timeout = timeout;
            if (Token != null)
            {
                cliente.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", Token);
            }
            string json = objeto.ToJson();
            var conteudoPost = new StringContent(json, Encoding.UTF8, "application/json");
            HttpResponseMessage resposta = await cliente.PostAsync(UrlServidor + rota, conteudoPost);
            var res = await resposta.Content.ReadAsStringAsync();
            try
            {
                cliente.Dispose();
                conteudoPost.Dispose();
            }
            catch (Exception)
            {

            }
            return JsonConvert.DeserializeObject(res);
            
        }
        public async Task<dynamic> Get(string rota)
        {
            HttpClient cliente = new HttpClient();
            cliente.Timeout = timeout;
            if(Token != null)
            {
                cliente.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", Token);
            }
            HttpResponseMessage resposta = await cliente.GetAsync(UrlServidor + rota);
            var res = await resposta.Content.ReadAsStringAsync();
            try
            {
                cliente.Dispose();
            }
            catch (Exception)
            {

            }
            return JsonConvert.DeserializeObject(res);
        }
        public async Task<dynamic> Get(string rota, List<Cabecalho> cabecalhos)
        {
            HttpClient cliente = new HttpClient();
            cliente.Timeout = timeout;
            if (Token != null)
            {
                cliente.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", Token);
            }
            foreach(Cabecalho cabecalho in cabecalhos)
            {
                cliente.DefaultRequestHeaders.Add(cabecalho.Key, cabecalho.Value);
            }
            HttpResponseMessage resposta = await cliente.GetAsync(UrlServidor + rota);
            var res = await resposta.Content.ReadAsStringAsync();
            try
            {
                cliente.Dispose();
            }
            catch (Exception)
            {

            }
            return JsonConvert.DeserializeObject(res);
        }
    }
}
