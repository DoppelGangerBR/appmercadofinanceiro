﻿using Xamarin.Forms;
using calculadora.Views;
namespace calculadora
{
    public partial class App : Application
    {
        public App()
        {
            Syncfusion.Licensing.SyncfusionLicenseProvider.RegisterLicense("MjA1NzQyQDMxMzcyZTM0MmUzMGVJNkU4d0Y3ZndQUlFKOTljZTNRTUllcGNJYmFxQVFyU2xPdGJvTnZOQWM9");
            InitializeComponent();
            MainPage = new NavigationPage(new Principal());
        }
        //AlphaVantage Access key: RLLB0LA8QEB8LMNQ
        //Acoes do Brasil possuem .SAO na frente
        //OIBR3 = OIBR3.SAO
        //ID Aplicativo
        //ca-app-pub-5291131975913419~6352792145
        //ID do bloco
        //ca-app-pub-5291131975913419/8787383796
        protected override void OnStart()
        {
            // Handle when your app starts
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }
    }
}
